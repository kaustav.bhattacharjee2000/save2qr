const {
  override,
  overrideDevServer,
  addWebpackPlugin,
} = require("customize-cra");
const CopyPlugin = require("copy-webpack-plugin");

const multipleEntry = require("react-app-rewire-multiple-entry")([
  {
    entry: "src/popup/popup.js",
    template: "public/popup.html",
    outPath: "/popup.html",
  },
  {
    entry: "src/options/options.js",
    template: "public/index.html",
    outPath: "/options.html",
  },
  // external routers here
  {
    entry: "src/popup/list.js",
    template: "public/list.html",
    outPath: "/list.html",
  },
  {
    entry: "src/popup/upload.js",
    template: "public/upload.html",
    outPath: "/upload.html",
  },
]);

const devServerConfig = () => (config) => {
  return {
    ...config,
    writeToDisk: true,
  };
};

const copyPlugin = new CopyPlugin({
  patterns: [
    { from: "public", to: "" },
    { from: "src/background.js", to: "" },
    { from: "src/contentScript.js", to: "" },
  ],
});

module.exports = {
  webpack: override(addWebpackPlugin(copyPlugin), multipleEntry.addMultiEntry),
  devServer: overrideDevServer(devServerConfig()),
};
