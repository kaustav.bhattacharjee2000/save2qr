import React from "react";
import ReactDOM from "react-dom";
import Upload from "./components/Upload";

ReactDOM.render(
  <React.StrictMode>
    <Upload />
  </React.StrictMode>,
  document.getElementById("root")
);
