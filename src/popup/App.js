import React from "react";
import Box from "@material-ui/core/Box";
import Header from "./components/Header";
import Footer from "./components/Footer";
import Body from "./components/Body";

const sizes = {
  maxWidth: "300px",
  titlebarMinHeight: "40px",
};

function App() {
  return (
    <Box width={sizes.maxWidth}>
      <Header maxWidth={sizes.maxWidth} minHeight={sizes.titlebarMinHeight} />
      <Body />
      <Footer maxWidth={sizes.maxWidth} minHeight={sizes.titlebarMinHeight} />
    </Box>
  );
}

export default App;
