import React from "react";
import ReactDOM from "react-dom";
import ListTabs from "./components/ListTabs";

ReactDOM.render(
  <React.StrictMode>
    <ListTabs />
  </React.StrictMode>,
  document.getElementById("root")
);
