import React from "react";
import {
  makeStyles,
  AppBar,
  Typography,
  Toolbar,
  Divider,
  Grid,
} from "@material-ui/core";
import { green } from "@material-ui/core/colors";

function Footer(props) {
  const classes = useStyles(props);

  return (
    <AppBar position="static" elevation={0} className={classes.appbar}>
      <Divider />
      <Toolbar className={classes.toolbar}>
        <Grid justify="space-between" container>
          <Grid item>
            <Typography variant="h6" className={classes.title}>
              Need help?
            </Typography>
          </Grid>
          <Grid item>
            <Typography variant="h6" className={classes.title}>
              version 0.1.0
            </Typography>
          </Grid>
        </Grid>
      </Toolbar>
    </AppBar>
  );
}

const useStyles = makeStyles((theme) => ({
  appbar: {
    backgroundColor: "white",
    top: "auto",
    bottom: 0,
  },
  title: {
    color: green[600],
    fontSize: 14,
  },
  toolbar: {
    minHeight: (props) => props.minHeight,
    height: (props) => props.minHeight,
  },
}));

export default Footer;
