import React from "react";
import { Typography, LinearProgress, Box } from "@material-ui/core";
import QrReader from "react-qr-reader";
import {
  parseFramesReducer,
  areFramesComplete,
  framesToData,
  progressOfFrames,
} from "qrloop";

export default class QRCodeScanner extends React.Component {
  state = {
    progress: 0,
  };

  frames = null;

  handleScan = (data) => {
    // ensures that scan only happens after user presses "Scan New"
    // console.log(this.props.getScanNew());
    if (this.props.getScanNew()) {
      if (data) {
        console.log("progress: ", this.state.progress);

        try {
          const frames = (this.frames = parseFramesReducer(this.frames, data));
          if (areFramesComplete(frames)) {
            // changed here
            this.props.onQRScanned(framesToData(frames).toString());
            this.setState({
              progress: 0,
            });
          } else {
            this.setState({
              progress: progressOfFrames(frames),
            });
          }
        } catch (e) {
          console.warn(e);
        }
      }
    }
  };

  handleError = (error) => {
    this.props.onQRScanError(error);
  };

  render() {
    return (
      <div
        style={{
          width: "100%",
          alignContent: "center",
          justifyContent: "center",
        }}
      >
        <QrReader
          delay={200} // for 5 fps
          onError={this.handleError}
          onScan={this.handleScan}
          style={{
            width: "70%",
            borderWidth: 10,
            borderColor: "blue",
            alignItems: "center",
            justifyContent: "center",
            alignSelf: "center",
            paddingLeft: "15%",
          }}
        />
        <Box display="flex" alignItems="center">
          <Box width="100%" mr={1}>
            <LinearProgress
              variant="determinate"
              value={this.state.progress * 100}
            />
          </Box>
          <Box minWidth={35}>
            <Typography variant="body2" color="textSecondary">
              {`${Math.round(this.state.progress * 100)}%`}
            </Typography>
          </Box>
        </Box>
      </div>
    );
  }
}
