/* global chrome */

import React from "react";
import {
  makeStyles,
  AppBar,
  Typography,
  Toolbar,
  IconButton,
} from "@material-ui/core";
import { green } from "@material-ui/core/colors";
import OpenInBrowserIcon from "@material-ui/icons/OpenInBrowser";

function Header(props) {
  const classes = useStyles(props);

  const onClickImport = () => {
    if (chrome.tabs) {
      chrome.tabs.create({
        url: "/upload.html",
      });
    }
  };

  return (
    <AppBar position="static" className={classes.appbar}>
      <Toolbar className={classes.toolbar}>
        <div
          style={{
            flex: 1,
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
          }}
        >
          <Typography variant="h6" className={classes.title}>
            Save2QR
          </Typography>
          <IconButton
            edge="end"
            aria-label="import-button"
            color="inherit"
            onClick={onClickImport}
          >
            <OpenInBrowserIcon style={{ color: "white" }} />
          </IconButton>
        </div>
      </Toolbar>
    </AppBar>
  );
}

const useStyles = makeStyles((theme) => ({
  appbar: {
    backgroundColor: green[600],
  },
  title: {
    fontSize: 18,
  },
  toolbar: {
    minHeight: (props) => props.minHeight,
    height: (props) => props.minHeight,
  },
}));

export default Header;
