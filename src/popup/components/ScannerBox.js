import * as React from "react";
import { makeStyles } from "@material-ui/core";

const scanBoxWidth = 250;

const BoxCorner = (props) => {
  const classes = useStyles();
  const additionalStyle = props.style || {};
  return <div className={classes.box} />;
};

export default function ScannerBox(props) {
  console.log("scannerbox");
  const classes = useStyles();

  return (
    <div className={classes.container}>
      <div className={classes.row}>
        <BoxCorner />
        <BoxCorner style={{ transform: [{ rotate: "90deg" }] }} />
      </div>
      <div className={classes.row}>
        <BoxCorner style={{ transform: [{ rotate: "-90deg" }] }} />
        <BoxCorner style={{ transform: [{ rotate: "180deg" }] }} />
      </div>
    </div>
  );
}

// const styles = {
//   container: {
//     height: scanBoxWidth,
//     width: scanBoxWidth,
//     justifyContent: "space-between",
//     backgroundColor: "black",
//   },
//   row: {
//     flexDirection: "row",
//     justifyContent: "space-between",
//   },
//   box: {
//     width: 50,
//     height: 50,
//     borderTopLeftRadius: 30,
//     borderTopWidth: 10,
//     borderLeftWidth: 10,
//     borderTopColor: "white",
//     borderLeftColor: "white",
//     borderRightColor: "transparent",
//     borderBottomColor: "transparent",
//   },
// };

const useStyles = makeStyles((theme) => ({
  container: {
    height: scanBoxWidth,
    width: scanBoxWidth,
    justifyContent: "space-between",
    backgroundColor: "black",
  },
  row: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  box: {
    backgroundColor: "white",
    width: 50,
    height: 50,
    borderTopLeftRadius: 30,
    borderTopWidth: 10,
    borderLeftWidth: 10,
    borderTopColor: "white",
    borderLeftColor: "white",
    borderRightColor: "transparent",
    borderBottomColor: "transparent",
  },
}));
