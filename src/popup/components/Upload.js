/* global chrome */

import React from "react";
import {
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  ListItemSecondaryAction,
  Collapse,
  makeStyles,
  Typography,
  IconButton,
  Button,
  Box,
} from "@material-ui/core";
import { green, red, blue } from "@material-ui/core/colors";
import CloseIcon from "@material-ui/icons/Close";
import ExpandLess from "@material-ui/icons/ExpandLess";
import ExpandMore from "@material-ui/icons/ExpandMore";
import AcUnitIcon from "@material-ui/icons/AcUnit";
import { useDropzone } from "react-dropzone";
import uuid from "uuid-random";
import QRCodeScanner from "./QRCodeScanner";

export default function Upload(props) {
  const [data, setData] = React.useState([]);
  const [webcamActive, setWebcamActive] = React.useState(false);
  const [showDialogueBox, setShowDialogueBox] = React.useState(false);
  const [dialogueStatus, setDialogueStatus] = React.useState({});
  const [scanNew, setScanNew] = React.useState(true);
  const classes = useStyles();

  const readFiles = (files) => {
    const _onload = (e, filename) => {
      let text = e.target.result;
      try {
        if (text) {
          const windowInfo = JSON.parse(text);
          setData((prev) =>
            prev.concat({
              id: prev.length,
              fromFile: filename,
              incognito: windowInfo.incognito || false,
              tabs: windowInfo.tabs || [],
            })
          );
          handleShowDialogue(false, `File upload successful !`);
        }
      } catch (e) {
        // console.error(e);
        handleShowDialogue(true, `Couldn't parse the file : ${filename}`);
      }
    };

    const _onerror = (filename) => {
      // console.error(`Couldn't read file : ${filename}`);
      handleShowDialogue(true, `Couldn't read the file : ${filename}`);
    };

    files.forEach((file) => {
      let reader = new FileReader();
      reader.onload = (e) => _onload(e, file.name);
      reader.onerror = () => _onerror(file.name);
      reader.readAsText(file);
    });
  };

  const onDrop = (files, event) => {
    readFiles(files);
  };

  const onQRScanned = (text) => {
    try {
      const windowInfo = JSON.parse(text);
      setData((prev) =>
        prev.concat({
          id: prev.length,
          fromFile: "Scanned",
          incognito: windowInfo.incognito || false,
          tabs: windowInfo.tabs || [],
        })
      );
      setScanNew(false);
      handleShowDialogue(false, "QR scan successful !");
    } catch (error) {
      onQRScanError(error);
    }
  };

  const onQRScanError = (error) => {
    console.log(error);
    handleShowDialogue(true, "Invalid QR code / data corrupted");
  };

  const handleImport = () => {
    // if running as an extension
    if (chrome.windows) {
      for (let i = 0; i < data.length; i++) {
        if (data[i].tabs.length > 0) {
          chrome.windows.create(
            {
              focused: false,
              incognito: data[i].incognito,
              url: data[i].tabs,
            },
            (newWindow) => {
              console.log("created new window with details:", newWindow);
            }
          );
        }
      }
    }
  };

  const handleRemove = (windowFilename) => {
    const removeIndex = data.findIndex(
      (window) => window.filename === windowFilename
    );
    data.splice(removeIndex, 1);
    setData([...data]);
    console.log(`removed : ${windowFilename}`);
  };

  const handleWebcamScan = () => {
    setWebcamActive(true);
  };

  const handleStopWebcam = () => {
    setWebcamActive(false);
  };

  const handleDialogueClose = () => {
    // same handler for both "close" and "dismiss" buttons
    setShowDialogueBox(false);
  };

  const handleShowDialogue = (_isError, _message) => {
    setDialogueStatus({ isError: _isError, message: _message });
    setShowDialogueBox(true);
  };

  const handleScanNew = () => {
    console.log("SCAN NEW");
    setScanNew(true);
    handleDialogueClose();
  };

  // this function is passed to QRCodeScanner to inform
  // if it needs to send the data back to parent
  const getScanNew = () => {
    return scanNew;
  };

  const {
    getRootProps,
    getInputProps,
    isDragActive,
    isDragAccept,
    isDragReject,
  } = useDropzone({
    accept: "application/json",
    onDrop: onDrop,
  });

  const RenderItem = (window) => {
    const [expandTabs, setExpandTabs] = React.useState(false);
    const primaryText = window.fromFile;
    const secondaryText = `${`${window.tabs.length} tabs `} ${
      window.incognito ? "- incognito" : ""
    }`;
    const _id = uuid();

    const handleExpand = () => {
      setExpandTabs(!expandTabs);
    };

    return (
      <React.Fragment key={`windowlist_${_id}`}>
        <ListItem>
          <ListItemIcon>
            <IconButton aria-label="expand" onClick={handleExpand}>
              {expandTabs ? <ExpandLess /> : <ExpandMore />}
            </IconButton>
          </ListItemIcon>
          <ListItemText primary={primaryText} secondary={secondaryText} />
          <ListItemSecondaryAction>
            <IconButton
              edge="end"
              aria-label="delete"
              onClick={() => handleRemove(window.fromFile)}
            >
              <CloseIcon />
            </IconButton>
          </ListItemSecondaryAction>
        </ListItem>
        {/*
         * Expanded tabs list below
         */}
        <Collapse in={expandTabs} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            {window.tabs.map((url, index) => (
              <ListItem key={`window_${uuid()}`} className={classes.nested}>
                <ListItemIcon>
                  <AcUnitIcon />
                </ListItemIcon>
                <ListItemText primary={url} />
              </ListItem>
            ))}
          </List>
        </Collapse>
      </React.Fragment>
    );
  };

  const displayList = (data) => (
    <List component="nav">
      {data.map((window) => (
        <RenderItem {...window} />
      ))}
    </List>
  );

  const displayDragAccept = (
    <Typography variant="h6">Drop the file(s) here :)</Typography>
  );

  const displayDragReject = (
    <Typography variant="h6">Invalid file format !</Typography>
  );

  const displayDragNotActive = (
    <Typography variant="h6">Drop file(s) here or click to upload</Typography>
  );

  const displayDialogueBox = () => {
    const onClickHandler = dialogueStatus.isError
      ? handleDialogueClose
      : webcamActive
      ? handleScanNew
      : handleDialogueClose;

    return (
      <Box
        display="flex"
        justifyContent="space-between"
        alignItems="center"
        border={1}
        borderColor={dialogueStatus.isError ? red[500] : green[500]}
        borderRadius="borderRadius"
        p={1}
        mb={1}
      >
        <Typography
          size={16}
          className={
            dialogueStatus.isError ? classes.red_msg : classes.green_msg
          }
        >
          {dialogueStatus.message}
        </Typography>
        <Button
          variant="contained"
          className={
            dialogueStatus.isError ? classes.error_btn : classes.success_btn
          }
          size="small"
          onClick={onClickHandler}
        >
          {dialogueStatus.isError
            ? "Dismiss"
            : webcamActive
            ? "Scan New"
            : "Close"}
        </Button>
      </Box>
    );
  };

  return (
    <div style={{ display: "flex", justifyContent: "center" }}>
      <div className={classes.root}>
        {/* 
          ERROR / SUCCESS dialog box
         */}
        {showDialogueBox && displayDialogueBox()}
        {webcamActive ? (
          <QRCodeScanner
            onQRScanned={onQRScanned}
            onQRScanError={onQRScanError}
            getScanNew={getScanNew}
          />
        ) : (
          <div className="container">
            <div {...getRootProps({ className: classes.dropzone })}>
              <input {...getInputProps()} />
              {isDragAccept && displayDragAccept}
              {isDragReject && displayDragReject}
              {!isDragActive && displayDragNotActive}
            </div>
          </div>
        )}
        <div className={classes.titleContainer}>
          <Typography variant="h5" className={classes.title}>
            Uploaded files
          </Typography>
          <div>
            {webcamActive ? (
              <Button
                className={classes.button_webcam_option}
                variant="outlined"
                onClick={handleStopWebcam}
              >
                Stop Webcam
              </Button>
            ) : (
              <Button
                className={classes.button_webcam_option}
                variant="outlined"
                onClick={handleWebcamScan}
              >
                Use Webcam
              </Button>
            )}
            <Button
              className={classes.button_import}
              variant="contained"
              onClick={handleImport}
            >
              Import
            </Button>
          </div>
        </div>
        {data.length > 0 ? (
          displayList(data)
        ) : (
          <Typography className={classes.title}>Nothing to import</Typography>
        )}
      </div>
    </div>
  );
}

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    maxWidth: theme.spacing(80),
  },
  nested: {
    paddingLeft: theme.spacing(8),
    paddingRight: theme.spacing(8),
  },
  hidden: {
    visibility: "hidden",
    display: "none",
  },
  title: {
    // paddingLeft: theme.spacing(4),
    paddingTop: theme.spacing(2),
  },
  titleContainer: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
  },
  dropzone: {
    flex: 1,
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    paddingLeft: theme.spacing(15),
    paddingRight: theme.spacing(15),
    paddingTop: theme.spacing(20),
    paddingBottom: theme.spacing(20),
    borderWidth: 2,
    borderRadius: 2,
    borderColor: "#eeeeee",
    borderStyle: "dashed",
    backgroundColor: "#fafafa",
    color: "#bdbdbd",
    outline: "none",
    transition: "border .24s ease-in-out",
  },
  success_btn: {
    backgroundColor: green[500],
    color: "white",
    "&:hover": {
      backgroundColor: green[700],
    },
  },
  error_btn: {
    backgroundColor: red[500],
    color: "white",
    "&:hover": {
      backgroundColor: red[700],
    },
  },
  button_import: {
    marginLeft: 15,
    backgroundColor: blue[300],
    color: "white",
    "&:hover": {
      backgroundColor: blue[600],
    },
  },
  button_webcam_option: {
    borderColor: blue[500],
    color: blue[700],
    "&:hover": {
      borderColor: blue[600],
    },
  },
  red_msg: {
    color: red[500],
  },
  green_msg: {
    color: green[500],
  },
}));
