// imported code from : https://github.com/gre/qrloop/blob/master/examples/web-text-exporter/src/QRCode.js

import React, { PureComponent } from "react";
import qrcode from "qrcode";

class QRCode extends PureComponent {
  componentDidMount() {
    this.draw();
  }

  componentDidUpdate() {
    this.draw();
  }

  canvas = React.createRef();

  draw() {
    const { data, size } = this.props;
    qrcode.toCanvas(this.canvas.current, data, { width: size });
  }

  render() {
    return <canvas style={{ cursor: "none" }} ref={this.canvas} />;
  }
}

export default QRCode;
