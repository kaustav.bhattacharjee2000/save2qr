/* global chrome */

import React, { useState, useEffect } from "react";
import {
  withStyles,
  makeStyles,
  Button,
  Checkbox,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Avatar,
  ListItemSecondaryAction,
  FormGroup,
  Typography,
} from "@material-ui/core";
import { blue, green } from "@material-ui/core/colors";
import { dataToFrames } from "qrloop";
import QRCodeLoop from "./QRCodeGenerator";

/**
 * Export file's json format :
 * {
 *   incognito : <boolean>
 *   tabs : <array of urls>
 * }
 *
 * same format for "tabsList"
 */

function ListTabs(props) {
  const [tabsList, setTabsList] = useState({});
  const [checked, setChecked] = useState([]);
  const [sendData, setSendData] = useState(null);
  const [frames, setFrames] = useState(null);
  const classes = useStyles();
  const maxLength = 60;
  const QRCodeSize = 300;

  useEffect(() => {
    if (chrome.tabs) {
      getTabsList((tabsData) => {
        setTabsList(tabsData);
      });
    }
  }, [setTabsList]);

  const getTabsList = (callback) => {
    chrome.windows.getCurrent((window) => {
      let tabsData = { incognito: window.incognito, tabs: [] };
      chrome.tabs.getAllInWindow(window.id, (tabs) => {
        tabs.forEach(({ id, title, url, favIconUrl }) => {
          tabsData.tabs.push({
            id,
            title,
            url,
            icon: favIconUrl,
          });
        });
        callback(tabsData);
      });
    });
  };

  const makeFrames = (data) => {
    let newFrames = dataToFrames(data, 100, 4);
    setFrames(newFrames);
  };

  const truncate = (str) => {
    return str.length > maxLength
      ? str.substring(0, maxLength - (3 + 1)) + "..."
      : str;
  };

  const getDataToExport = () => {
    let exportTabsList = JSON.stringify({
      incognito: tabsList.incognito,
      tabs: tabsList.tabs
        .filter((tab) => checked.includes(tab.id))
        .map((tab) => tab.url),
    });
    return exportTabsList;
  };

  const handleToggle = (value) => () => {
    const currentIndex = checked.indexOf(value);
    const newChecked = [...checked];
    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }
    setChecked(newChecked);
  };

  const handleSelectAll = () => {
    // if tabsList is not empty
    if (tabsList.tabs) {
      let newChecked = tabsList.tabs.map((tab) => tab.id);
      setChecked(newChecked);
    }
  };

  const handleClearAll = () => {
    setChecked([]);
  };

  const handleGenerateQR = () => {
    if (checked.length > 0) {
      let newData = getDataToExport();
      setSendData(newData);
      makeFrames(newData);
    } else {
      setSendData(null);
    }
  };

  const handleExport = () => {
    if (checked.length > 0) {
      let jsonData = getDataToExport();
      let blob = new Blob([jsonData], { type: "application/json" });
      let url = URL.createObjectURL(blob);
      let filename = "export.json";

      // if only running as extension
      if (chrome.downloads) {
        chrome.downloads.download({
          url: url,
          filename: filename,
        });
      }
    }
  };

  const showQRCodeUI = () => {
    if (sendData) {
      return (
        <div className={classes.qr}>
          <QRCodeLoop frames={frames} fps={5} size={QRCodeSize} />
        </div>
      );
    } else {
      return null;
    }
  };

  const showTopButtons = () => (
    <FormGroup row className={classes.form}>
      <div>
        <CustomButton
          variant="contained"
          size="small"
          onClick={handleGenerateQR}
          marginRight={true}
        >
          Generate QR
        </CustomButton>
        <CustomButton
          variant="contained"
          bgcolor={green[400]}
          bgcolorhover={green[700]}
          size="small"
          onClick={handleExport}
        >
          Export
        </CustomButton>
      </div>
      <div>
        <Button color="primary" onClick={handleSelectAll}>
          Select All
        </Button>
        <Button color="primary" onClick={handleClearAll}>
          Clear All
        </Button>
      </div>
    </FormGroup>
  );

  const showList = () => (
    <List dense>
      {tabsList.tabs &&
        tabsList.tabs.map((tab) => {
          const labelId = `labelId-${tab.id}`;

          return (
            <ListItem key={tab.id}>
              <ListItemIcon>
                <Avatar
                  variant="square"
                  src={tab.icon}
                  alt="icon"
                  className={classes.icon}
                />
              </ListItemIcon>
              <ListItemText
                id={labelId}
                primary={truncate(tab.title)}
                secondary={truncate(tab.url)}
              />
              <ListItemSecondaryAction>
                <Checkbox
                  color="primary"
                  edge="end"
                  onChange={handleToggle(tab.id)}
                  checked={checked.indexOf(tab.id) !== -1}
                  inputProps={{ "aria-labelledby": labelId }}
                />
              </ListItemSecondaryAction>
            </ListItem>
          );
        })}
    </List>
  );

  return (
    <div style={{ display: "flex", justifyContent: "center" }}>
      <div className={classes.root}>
        <Typography className={classes.title}>
          Select the tabs from below and click on generate button to get the QR
          code
        </Typography>
        {showQRCodeUI()}
        {showTopButtons()}
        {showList()}
      </div>
    </div>
  );
}

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    maxWidth: theme.spacing(80),
  },
  form: {
    justifyContent: "space-between",
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(1),
  },
  icon: {
    width: theme.spacing(3),
    height: theme.spacing(3),
  },
  title: {
    textAlign: "center",
    paddingBottom: theme.spacing(2),
  },
  qr: {
    display: "flex",
    justifyContent: "center",
    paddingBottom: 10,
    height: 300,
  },
}));

const CustomButton = withStyles((theme) => ({
  root: {
    color: "white",
    backgroundColor: (props) => props.bgcolor || blue[400],
    "&:hover": {
      backgroundColor: (props) => props.bgcolorhover || blue[600],
    },
    marginLeft: (props) => (props.marginLeft ? theme.spacing(1) : 0),
    marginRight: (props) => (props.marginRight ? theme.spacing(1) : 0),
  },
}))(Button);

export default ListTabs;
