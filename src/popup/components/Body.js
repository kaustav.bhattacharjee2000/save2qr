/* global chrome */

import React, { useState, useEffect } from "react";
import {
  makeStyles,
  Typography,
  Box,
  Button,
  withStyles,
} from "@material-ui/core";
import { green } from "@material-ui/core/colors";
import { dataToFrames } from "qrloop";
import QRCodeLoop from "./QRCodeGenerator";

export default function Body(props) {
  const [frames, setFrames] = useState(null); // frames of the tab's url
  const [showQR, setShowQR] = useState(false);
  const classes = useStyles(props);
  const QRCodeSize = 200;

  useEffect(() => {
    // if we are running inside extension
    if (chrome.tabs) {
      chrome.tabs.query({ active: true, currentWindow: true }, (tabs) => {
        // setUrl(tabs[0].url);
        const dataToSend = JSON.stringify({
          incognito: tabs[0].incognito,
          tabs: [tabs[0].url],
        });
        makeFrames(dataToSend);
      });
    }
  }, []);

  const makeFrames = (data) => {
    let newFrames = dataToFrames(data, 100, 4);
    setFrames(newFrames);
  };

  const onCurrentTabClick = () => {
    setShowQR(true);
  };

  /*
  incognito (for window), iconurl, url, title, id (only listing purpose) 
  */

  const onCurrentWindowClick = () => {
    // if we are running inside extension
    if (chrome.tabs) {
      chrome.tabs.create({ url: "/list.html" });
    }
  };

  const CurrentTabQR = () => (
    <React.Fragment>
      <Typography className={classes.title}>QR code for current tab</Typography>
      <div className={classes.qr}>
        {/* <QRCode value={url} level={"M"} size={200} /> */}
        <QRCodeLoop frames={frames} fps={5} size={QRCodeSize} />
      </div>
    </React.Fragment>
  );

  // button onhover style not changing -> fix that
  const ChooseWhichToGenerate = () => (
    <React.Fragment>
      <Typography className={classes.title}>Generate QR code for :</Typography>
      <div className={classes.buttonContainer}>
        <CustomButton
          variant="contained"
          color="primary"
          size="small"
          onClick={onCurrentTabClick}
        >
          Current tab
        </CustomButton>
        <CustomButton
          variant="contained"
          color="primary"
          size="small"
          onClick={onCurrentWindowClick}
        >
          Current window
        </CustomButton>
      </div>
    </React.Fragment>
  );

  return (
    <Box className={classes.container}>
      {showQR ? <CurrentTabQR /> : <ChooseWhichToGenerate />}
    </Box>
  );
}

const useStyles = makeStyles((theme) => ({
  title: {
    fontSize: 14,
    textAlign: "center",
  },
  container: {
    padding: theme.spacing(1),
  },
  qr: {
    display: "flex",
    justifyContent: "center",
    // paddingTop: theme.spacing(1),
    // paddingBottom: theme.spacing(1),
  },
  buttonContainer: {
    display: "flex",
    justifyContent: "space-evenly",
    paddingTop: theme.spacing(1),
    paddingBottom: theme.spacing(1),
  },
}));

const CustomButton = withStyles((theme) => ({
  root: {
    color: "white",
    backgroundColor: green[400],
    "&:hover": {
      backgroundColor: green[600],
    },
  },
}))(Button);
