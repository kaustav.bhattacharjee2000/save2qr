# Save2QR

A browser extension made with ReactJS to generate QR Codes for a single/multiple browser tabs, and scan them using [Save2QR mobile app](https://gitlab.com/kaustav.bhattacharjee2000/save2qr-app) to open them on your phone.

### Features

- Can generate a single animated QR Code for the any number of tabs in window
- Scan QR codes from the mobile app to transfer previous scanned tabs back to your pc, via webcam
- Option of saving the tabs locally in a json file
- Open up the tabs from previously saved json file. Multiple files can be uploaded, each of which will be opened in separate windows.
- Supports both incognito and regular tabs, i.e, opens up incognito tabs only in an incognito window

### Advantages

- No login required to save data in cloud, everything is local
- No internet required

### Run

If you want to quickly test it out, then download the extension from [here](https://drive.google.com/drive/folders/1y0kOlzeVPzJ09wj6EQFb8NyloXuWvOCq?usp=sharing), extract the `browser-extension.zip` file, and you will find an `extension` folder within that. Take a note of the folder's location and follow the steps below :

- Open up chrome browser, type `chrome://extensions` and press enter
- Enable **developer mode** on top right side
- Click on **Load unpacked**, search for the folder **extension**, and click **open**
- To pin the extension, click on the **zigsaw puzzle icon** on the right side of tab's search bar, then click on **pin icon** besides the Save2QR extension.
- To enable it in incognito/in-private mode, go to `chrome://extensions`, click on **details button** of Save2QR extension, scroll down to **Allow in incognito**, and enable it

### Build

Steps:

- Clone the repo
- Install the packages using `npm install`
- Run `npm run build`
- Follow the steps given in **Run** section above. Note that the newly created **build** folder is referred to as the **extension** folder

### Screenshots

- **Extension's popup screen**

  ![extension popup](./images/front.jpg)



- **Animated QR code for current tab**

  ![animated qr](./images/qr.jpg)



- **Exporting multiple tabs**

  ![export tabs section](./images/export.jpg)



- **Importing tabs by file upload or scanning via webcam from Save2QR app**

  ![import tabs section](./images/import.jpg)
